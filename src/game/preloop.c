#include "planetary.h"
#include "globalvars.h"
#include "gamevars.h"

// Game state management
int _gamestate_current 		= 0;		// current gamestate ID
int	_gamestate_previous 	= -1;		// previous gamestate ID
int	_gamestate_next			= 0;		// next gamestate ID
bool _gamestate_passed		= false;
gamestate_t* gamestate_accountant[_STATE_END+1]; 	// tracks registered gamestates addresses

// In-game
gamestate_t* 	ingame_state;
cmap_t* 		cm_height;
cmap_t* 		cm_relief;
cmap_t*			cm_ocean;

// Sound effects
sound_t*		buttonpress_sound;
sound_t*		buttonhover_sound;
music_t*		mainmenu_music;
playlist_t* 	ingame_playlist;

// Main menu
gamestate_t* 	mainmenu_state;
button_t*		mainmenu_button_new;
button_t*		mainmenu_button_load;
button_t*		mainmenu_button_settings;
button_t*		mainmenu_button_quit;
texture_t*		mainmenu_texture_background;
texture_t*		mainmenu_texture_title;
texture_t*		mainmenu_texture_author;

// Loading screen
gamestate_t*	loading_state;
texture_t*		loading_texture_background;
texture_t*		loading_texture_message;
texture_t*		loading_texture_status;
texture_t*		loading_texture_percent;
int				loadingpct = 0;
 
// Different objects to handle
texture_t* 		init_text;

// Timing/debug variables
bool 		showDebug = true;	// Show debug overlay?
dictbox_t* 	debugDictbox;

// Input variables
SDL_Event e;
int mouseX, mouseY;			// Current mouse coords
int mouseX_old, mouseY_old; // Previous mouse coords
int mouse1, mouse2, mouse3; // Current mouse button states
Uint8* keys;

// Things to do before starting the main loop
void preLoop() {
	// Prepare important stuff ----------------------------------------

	init_text = texture_loadFromText(gRenderer, "Loading...", font_big, color_white);
	texture_setX(init_text, (float) (windowWidth  / 2 - texture_getWidth(init_text)/2));
	texture_setY(init_text, (float) (windowHeight / 2 - texture_getHeight(init_text)/2));

	SDL_SetRenderDrawColor( gRenderer, color_background.r, color_background.g, color_background.b, color_background.a ); 
	SDL_RenderClear(gRenderer);
	texture_render(init_text);
	SDL_RenderPresent(gRenderer);

	// Important game stuff ----------------------------------------

	printf(PRT_MSG"Debug menu \n");

	debugDictbox = dictbox_init(7, 100, 5, 5, font_small, color_black, color_black, color_white_50 );

	dictbox_setLine(debugDictbox, "FPS", &intFPS);
	dictbox_setLine(debugDictbox, "VSync", &vsync);
	dictbox_setLine(debugDictbox, "MouseX", &mouseX);
	dictbox_setLine(debugDictbox, "MouseY", &mouseY);
	dictbox_setLine(debugDictbox, "Mouse1", &mouse1);
	dictbox_setLine(debugDictbox, "Mouse2", &mouse2);
	dictbox_setLine(debugDictbox, "G-state", &_gamestate_current);

	// Normal game stuff ----------------------------------------

	printf(PRT_MSG"Game assets, objects, states \n");

	// 		Global game stuff

	buttonpress_sound = sound_loadFromFile("./res/audio/button_press.wav");
	buttonhover_sound = sound_loadFromFile("./res/audio/button_hover.wav");

	cm_height = cmap_init(CMAP_SCHEME_MARS, false);
	cmap_setInterpolation(cm_height, CMAP_INTERP_LINEAR);

	cm_relief = cmap_init(CMAP_SCHEME_GREYS, false);
	cmap_setInterpolation(cm_relief, CMAP_INTERP_LINEAR);

	cm_ocean = cmap_init(CMAP_SCHEME_OCEAN, true);
	cmap_setInterpolation(cm_ocean, CMAP_INTERP_LINEAR);

	//		Main menu

	int mm_buttony = GYFP(0.666);

	mainmenu_state = gamestate_init(STATE_MAINMENU);

	mainmenu_texture_background = texture_loadFromFile(gRenderer, "./res/textures/mm_background.jpg");
	texture_scaleToWindow(mainmenu_texture_background);
	gamestate_registerTexture(mainmenu_state, mainmenu_texture_background,1);

	mainmenu_texture_title = texture_loadFromText(gRenderer, "A game without a name", font_big, color_bluewhite);
	texture_setX(mainmenu_texture_title, windowWidth / 2 - texture_getWidth(mainmenu_texture_title) / 2);
	texture_setY(mainmenu_texture_title, GYFP(0.333));
	gamestate_registerTexture(mainmenu_state, mainmenu_texture_title,2);

	mainmenu_texture_author = texture_loadFromText(gRenderer, "by Harrison Nicholls", font_small, color_white_50);
	texture_setX(mainmenu_texture_author, windowWidth - texture_getWidth(mainmenu_texture_author) - 3);
	texture_setY(mainmenu_texture_author, windowHeight - texture_getHeight(mainmenu_texture_author) - 3);
	gamestate_registerTexture(mainmenu_state, mainmenu_texture_author,2);

	mainmenu_button_new = button_init(gRenderer, GXFP(0.2) - 70, mm_buttony, "New", font_small);
	gamestate_registerButton(mainmenu_state, mainmenu_button_new,3);

	mainmenu_button_load = button_init(gRenderer, GXFP(0.4) - 70, mm_buttony, "Load", font_small);
	gamestate_registerButton(mainmenu_state, mainmenu_button_load,3);

	mainmenu_button_settings = button_init(gRenderer, GXFP(0.6) - 70, mm_buttony, "Settings", font_small);
	gamestate_registerButton(mainmenu_state, mainmenu_button_settings,3);

	mainmenu_button_quit = button_init(gRenderer,GXFP(0.8) - 70, mm_buttony, "Quit", font_small);
	gamestate_registerButton(mainmenu_state, mainmenu_button_quit,3);

	gamestate_registerSound(mainmenu_state, buttonpress_sound);
	gamestate_registerSound(mainmenu_state, buttonhover_sound);

	gamestate_setReady(STATE_MAINMENU);
	gamestate_makeCurrent(STATE_MAINMENU);
	gamestate_setNext(STATE_MAINMENU);

	mainmenu_music = music_loadFromFile("./res/audio/holst/jupiter.wav");
	music_start(mainmenu_music,-1);

	//	 	Loading screen

	loading_state = gamestate_init(STATE_LOADING);

	loading_texture_background = texture_loadFromFile(gRenderer, "./res/textures/mars_background.jpg");
	texture_scaleToWindow(loading_texture_background);
	gamestate_registerTexture(loading_state, loading_texture_background,1);

	loading_texture_message = texture_loadFromText(gRenderer, "Loading game...", font_big, color_white);
	texture_setX(loading_texture_message, 50);
	texture_setY(loading_texture_message, windowHeight - 100);
	gamestate_registerTexture(loading_state, loading_texture_message,2);

	loading_texture_status = texture_loadFromText(gRenderer, "Begin loading", font_small, color_white);
	texture_setX(loading_texture_status, 50);
	texture_setY(loading_texture_status, windowHeight - 70);
	gamestate_registerTexture(loading_state, loading_texture_status,2);

	loading_texture_percent = texture_loadFromText(gRenderer, "100%", font_small, color_white);
	texture_setX(loading_texture_percent, 50);
	texture_setY(loading_texture_percent, windowHeight - 50);
	gamestate_registerTexture(loading_state, loading_texture_percent,2);

	gamestate_setReady(STATE_LOADING);

	//		In game 

	ingame_state = gamestate_init(STATE_INGAME);

	for (int i = 0; i < CHUNKS_NX*CHUNKS_NY; i++){
		ingame_pixelarrays_relief[i] = pixelarray_init(gRenderer, CHUNKS_L, CHUNKS_L);
		ingame_pixelarrays_dem[i] = pixelarray_init(gRenderer,CHUNKS_L,CHUNKS_L);
	}

	printf(PRT_MSG"Done initialising chunks \n");

	ingame_playlist = playlist_init();
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/mars.wav"));
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/mercury.wav"));
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/neptune.wav"));
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/saturn.wav"));
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/uranus.wav"));
	playlist_registerMusic(ingame_playlist, music_loadFromFile("./res/audio/holst/venus.wav"));

}