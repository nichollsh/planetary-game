#include "planetary.h"
#include "globalvars.h"

enum tex_types { TYPE_IMAGE , TYPE_WORD };

/**
 * A texture is an image (or text) object which can be rendered to the screen.
 * Text can be rendered to the screen using textures, as well as images stored in the filesystem.
 * Pixelarrays also depend on textures.
 */


/**
 * Loads texture from filesystem into texture structure 
 * @param gRenderer SDL Renderer that the texture will be rendered by
 * @param path		Path to location of file containing texture (in filesystem)
 */
texture_t* texture_loadFromFile(SDL_Renderer* gRenderer, char* path ) {

	texture_t* tex = (texture_t*) malloc(sizeof(texture_t));
	if (tex == NULL) {
		printf("Error: Could not a allocate memory for texture! \n");
		exit(EXIT_FAILURE);
	}

	tex->gRenderer = gRenderer;

	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load( path );

	if( loadedSurface == NULL ) {
		printf(PRT_WARN"Unable to load image %s! '%s'\n", path, IMG_GetError() );
	} else {
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF ) );
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );

		if( newTexture == NULL ) {
			printf(PRT_WARN"Unable to create texture from %s! '%s'\n", path, SDL_GetError() );
		} else {
			tex->pix_width = loadedSurface->w;
			tex->pix_height = loadedSurface->h;

			tex->width = tex->pix_width;
			tex->height = tex->pix_height;

			tex->x = 0;
			tex->y = 0;
		}

		SDL_FreeSurface( loadedSurface );
	}

	tex->mTexture 	= newTexture;
	tex->clip 		= NULL;
	tex->flip 		= SDL_FLIP_NONE;
	tex->angle 		= 0;
	tex->tex_type	= TYPE_IMAGE;

	if (tex->mTexture == NULL) {
		printf(PRT_WARN"Failed to render image \n");
	}

	return tex;
}

/**
 * Converts a string into a texture for rendering. Can then be treated like image texture.
 * @param gRenderer 	SDL Renderer that the texture will be rendered by
 * @param tex			texture_t struct that the texture will be stored in, alongside other data
 * @param textureText	Text to be displayed
 * @param gFont			TTF_Font to be used
 * @param textColor		Color to render text as
 */
texture_t* texture_loadFromText(SDL_Renderer* gRenderer, char* textureText, TTF_Font* gFont, SDL_Color textColor ) {

	texture_t* tex = (texture_t*) malloc(sizeof(texture_t));

	tex->gRenderer = gRenderer;

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Blended( gFont, textureText, textColor );
	if( textSurface == NULL ) {
		printf(PRT_WARN"Unable to render text surface! '%s'\n", TTF_GetError() );
	} else {
		//Create texture from surface pixels
        tex->mTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( tex->mTexture == NULL ) {
			printf(PRT_WARN"Unable to create texture from rendered text! '%s'\n", SDL_GetError() );
		} else {
			//Get image dimensions
			tex->pix_width = textSurface->w;
			tex->pix_height = textSurface->h;
			tex->width = tex->pix_width;
			tex->height = tex->pix_height;
			tex->x = 0;
			tex->y = 0;

		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}

	if (tex->mTexture == NULL) {
		printf(PRT_WARN"Failed to render text \n");
	}

	tex->clip 		= NULL;
	tex->flip 		= SDL_FLIP_NONE;
	tex->angle 		= 0;
	tex->tex_type	= TYPE_WORD;

	return tex;
	
}

/**
 * Create copy of existing SDL texture
 * @param gRenderer 	SDL Renderer that the texture will be rendered by
 * @param mtex			Existing SDL texture
 * @param pix_w			Total pixel width
 * @param pix_h			Total pixel height
 */
texture_t* texture_createImitation(SDL_Renderer* gRenderer, SDL_Texture* mtex, int pix_w, int pix_h) {

	texture_t* tex = (texture_t*) malloc(sizeof(texture_t));
	tex->mTexture = (SDL_Texture*) malloc(sizeof(SDL_Texture*));

	tex->gRenderer = gRenderer;

	if (mtex == NULL) {
		printf(PRT_WARN"Failed to copy texture \n");
	} else {
		tex->mTexture = mtex;
		// set pointer to be the same; the texture's data isn't copied
		// when the original updates, this updates.
	}

	tex->pix_width = pix_w;
	tex->pix_height = pix_h;
	tex->width = pix_w;
	tex->height = pix_h;
	tex->scalex = 1;
	tex->scaley = 1;

	tex->x = 0;
	tex->y = 0;

	tex->clip 		= NULL;
	tex->flip 		= SDL_FLIP_NONE;
	tex->angle 		= 0;
	tex->tex_type	= 0;

	return tex;
	
}

void texture_updateText(texture_t* tex, char* text, TTF_Font* gFont, SDL_Color color) {

	if (tex == NULL) {
		printf(PRT_WARN"Cannot update the text of an uninitialised texture\n");
		return;
	}

	if (tex->tex_type == TYPE_IMAGE) {
		printf(PRT_WARN"Cannot update the text of an image texture\n");
		return;
	} 

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Blended( gFont, text, color );
	if( textSurface == NULL ) {
		printf(PRT_WARN"Unable to render text surface! '%s'\n", TTF_GetError() );
	} else {
		//Create texture from surface pixels
		SDL_DestroyTexture(tex->mTexture);
        tex->mTexture = SDL_CreateTextureFromSurface( tex->gRenderer, textSurface );
		if( tex->mTexture == NULL ) {
			printf(PRT_WARN"Unable to create texture from rendered text! '%s'\n", SDL_GetError() );
		} else {
			//Get image dimensions
			tex->pix_width = textSurface->w;
			tex->pix_height = textSurface->h;
			tex->width = tex->pix_width;
			tex->height = tex->pix_height;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}

	if (tex->mTexture == NULL) {
		printf(PRT_WARN"Failed to render text \n");
	}

}

/**
 * Frees memory being used by texture
 * @param tex	texture structure
 */
void texture_free(texture_t* tex) {
	//Free texture if it exists
	if( tex->mTexture != NULL ) {
		SDL_DestroyTexture( tex->mTexture );
		tex->mTexture = NULL;
		tex->pix_width = 0;
		tex->pix_height = 0;
	}
	free(tex);
}


/**
 * Sets texture x coordinate (top left corner of texture)
 * @param tex	texture structure
 * @param x		new x position
 */
void texture_setX(texture_t* tex, float x ) {
	tex->x = x;
}

/**
 * Offsets texture x coordinate (top left corner of texture)
 * @param tex	texture structure
 * @param x		new x position
 */
void texture_transX(texture_t* tex, float dx ) {
	tex->x += dx;
}

/**
 * Sets texture y coordinate (top left corner of texture)
 * @param tex	texture structure
 * @param y	new y position
 */
void texture_setY(texture_t* tex, float y ) {
	tex->y = y;
}

/**
 * Offsets texture y coordinate (top left corner of texture)
 * @param tex	texture structure
 * @param y		new y position
 */
void texture_transY(texture_t* tex, float dy ) {
	tex->y += dy;
}

/**
 * Modulates texture colour
 * @param tex	texture structure
 * @param col	color to modulate by
 */
void texture_setColor(texture_t* tex, SDL_Color col ) {
	SDL_SetTextureColorMod( tex->mTexture, col.r, col.g, col.b );
}

/**
 * Sets blend mode of texture; enables alpha, etc.
 * @param tex		texture structure
 * @param blending	blend mode
 */
void texture_setBlendMode(texture_t* tex, SDL_BlendMode blending ) {
	//Set blending function
	SDL_SetTextureBlendMode( tex->mTexture, blending );
}

/**
 * Modulates alpha value of texture (transparency)
 * @param tex		texture structure
 * @param alpha		alpha value
 */
void texture_setAlpha(texture_t* tex, Uint8 alpha ) {
	SDL_SetTextureAlphaMod( tex->mTexture, alpha );
}

/**
 * Scales texture about its origin (top left)
 * @param tex		texture structure
 * @param scalex	scale factor in x-direction
 * @param scaley	scale factor in y-direction
 */
void texture_setScale(texture_t* tex, float scalex, float scaley) {
	
	// if (scalex < TEXTURE_S_MIN) scalex = TEXTURE_S_MIN;
	// if (scaley < TEXTURE_S_MIN) scaley = TEXTURE_S_MIN;

	tex->scalex = scalex;
	tex->scaley = scaley;

	tex->width  = (int) (tex->pix_width  * scalex);
	tex->height = (int) (tex->pix_height * scaley);
}

/**
 * Scale texture about the position (u,v) such that it expands from that point, by translating texture as required
 * @param tex		texture structure
 * @param scalex	scale factor in x-direction
 * @param scaley	scale factor in y-direction
 * @param u			origin of scale transformation (x-dimension)
 * @param v			origin of scale transformation (y-dimension)
 */
void texture_setScaleAt(texture_t* tex, float scalex, float scaley, float u, float v) {

	float old_width  = (float) tex->width;
	float old_height = (float) tex->height;

	if (scalex < TEXTURE_S_MIN) scalex = TEXTURE_S_MIN;
	if (scaley < TEXTURE_S_MIN) scaley = TEXTURE_S_MIN;

	tex->scalex = scalex;
	tex->scaley = scaley;

	tex->width  = (int) (tex->pix_width  * scalex);
	tex->height = (int) (tex->pix_height * scaley);

	// Set new (x,y) coordinates for texture such that we zoom in on (u,v)
	tex->x = u + (tex->pix_width  * scalex / old_width)  * (tex->x - u);
	tex->y = v + (tex->pix_height * scaley / old_height) * (tex->y - v);

}

/**
 * Set clipping to use when rendering texture
 * @param tex			texture_t struct that the texture is stored in
 * @param clip			part of texture to be rendered (allows for cropping)
 */
void texture_setClip(texture_t* tex, SDL_Rect* clip) {
	tex->clip = clip;
}

/**
 * Set angle of rotation
 * @param tex			texture_t struct that the texture is stored in
 * @param angle			rotation angle (degrees, clockwise)
 */
void texture_setAngle(texture_t* tex, double angle) {
	tex->angle = angle;
}

/**
 * Set flip operation to perform on texture
 * @param tex			texture_t struct that the texture is stored in
 * @param flip  		flip operation to perform
 */
void texture_setFlip(texture_t* tex, SDL_RendererFlip flip) {
	tex->flip = flip;
}

/**
 * Render the texture stored in texture structure to renderer
 * @param tex			texture_t struct that the texture is stored in
 */
void texture_render(texture_t* tex) {

	bool shouldRender = TEXTURE_ENABLE;

	if (TEXTURE_HOOF) {
		if (tex->x > windowWidth)	shouldRender = false; // right
		if (tex->y > windowHeight)	shouldRender = false; // bottom
		if (tex->x+tex->width < 0)	shouldRender = false; // left
		if (tex->y+tex->height < 0)	shouldRender = false; // top
	}

	if (shouldRender) {
		//Set rendering space and render to screen
		SDL_Rect renderQuad = { (int) roundf(tex->x) , (int) roundf(tex->y), tex->width, tex->height };
		SDL_Rect* clipQuad;

		// Set clip rendering dimensions
		if (TEXTURE_CLIP) {
			clipQuad = tex->clip;
			if( tex->clip != NULL ) {
				printf(PRT_WARN"Clipping enabled?! %p \n", &tex->clip);
				renderQuad.w = clipQuad->w;
				renderQuad.h = clipQuad->h;
			}
		} else {
			clipQuad = NULL;
		}

		//Render to screen
		SDL_RenderCopyEx( tex->gRenderer, tex->mTexture, clipQuad, &renderQuad, tex->angle, NULL, tex->flip );
	} 
}

/**
 * Render existing texture again but at a different location
 * @param gRenderer		SDL Renderer that the texture will be rendered by
 * @param mTexture		SDL Texture that is to be copied
 * @param x				x position to render texture at (in window coordinates - top left origin)
 * @param y				y position to render texture at (in window coordinates - top left origin)
 * @param w				width of clone
 * @param h				height of clone
 */
void texture_renderClone(SDL_Renderer* gRenderer, SDL_Texture* mTexture, float x, float y, int w, int h){
	SDL_Rect renderQuad = { (int) roundf(x) , (int) roundf(y), w, h };
	SDL_RenderCopyEx( gRenderer, mTexture, NULL, &renderQuad, 0, NULL, SDL_FLIP_NONE );
}

int texture_getWidth(texture_t* tex) {
	return tex->width;
}

int texture_getHeight(texture_t* tex) {
	return tex->height;
}

float texture_getScaleX(texture_t* tex) {
	return tex->scalex;
}

float texture_getScaleY(texture_t* tex) {
	return tex->scaley;
}


void texture_scaleToWindow(texture_t* tex) {

	float sf = 1.0;

	if (tex->pix_height / tex->pix_width < windowHeight / windowWidth) {
		sf = (float) windowHeight / (float) tex->pix_height;
		texture_setScale(tex, sf, sf);
	} else {
		sf = (float) windowWidth / (float) tex->pix_width;
		texture_setScale(tex, sf, sf);
	}

}