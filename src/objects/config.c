#include "planetary.h"

config_t* config_read(char* path) {

	config_t* config = (config_t*) malloc(sizeof(config_t));


	FILE* file = fopen(path, "r");

	if (file == NULL) {
		printf(PRT_ERROR"Could not open config file\n");
		exit(EXIT_FAILURE);
	}

	char key[STRLEN];
	char val[STRLEN];

	while(fscanf(file, "%s %255[^\n]\n", key, val) != EOF) { 

		if (key[0] != '#') {
			if (strcmp(key, "window_width") == 0) {
				sscanf(val, "%d", &config->windowWidth);
			} else if (strcmp(key, "window_height") == 0) {
				sscanf(val, "%d", &config->windowHeight);
			} else if (strcmp(key, "window_title") == 0) {
				strcpy(config->windowTitle,val);
			} else if (strcmp(key, "vertical_sync") == 0) {
				sscanf(val, "%d", &config->vsync);
			} else if (strcmp(key, "fps_max") == 0) {
				sscanf(val, "%d", &config->fpsMax);
			} else {
				printf(PRT_WARN"Unrecognised config key (key = '%s' ; val = '%s') \n", key, val);
			}
		} else {
			// printf("Config contains comment! \n");
		}

	}  
	fclose(file);  

	return config;
}


