#include "planetary.h"
#include "globalvars.h"
#include "gamevars.h"

/**
 * Gamestates provide a way of collecting objects which are displayed/used at the same time into groups. 
 * They allow many things to be updated and rendered with just 2 function calls.
 * They allow contextualisation of objects, rather than a massive list of them.
 * They make memory management easier.
 * Within a gamestate, each type of object (e.g. buttons) will be stored in a pool (an array of that object).
 * Gamestates are tracked by the 'gamestate accountant', which stores memory addresses to states, making switching easier.
 */

extern gamestate_t* gamestate_accountant[_STATE_END+1];
extern int			_gamestate_current;
extern int 			_gamestate_previous;
extern int			_gamestate_next;

/**
 * Create new gamestate structure; returns pointer to malloc'd structure
 */
gamestate_t* gamestate_init(int ID) {

	gamestate_t* gs = (gamestate_t*) malloc(sizeof(gamestate_t));
	
	gs->button_pop = 0;   		// Pool population
	memset(gs->buttonLayers,0,MAX_BUTTONS*sizeof(int));

	gs->texture_pop = 0;
	memset(gs->textureLayers,0,MAX_TEXTURES*sizeof(int));

	gs->pixelarray_pop = 0;
	memset(gs->pixelarrayLayers,0,MAX_PIXELARRAYS*sizeof(int));

	gs->rectangle_pop = 0;
	memset(gs->rectangleLayers,0,MAX_RECTANGLES*sizeof(int));

	gs->sound_pop = 0;

	gs->_ready = false;			// Is this gamestate prepared for use?

	if (ID <= _STATE_START || ID >= _STATE_END) {
		printf(PRT_MSG"Created gamestate with perculiar intent (ID = %d) \n", ID);
	} else {
		if (gamestate_accountant[ID] != NULL) {
			printf(PRT_WARN"Overwriting previous gamestate (ID = %d)\n", ID);
		}
	}

	printf(PRT_MSG"Registered gamestate %d with accountant \n", ID);
	gamestate_accountant[ID] = gs;
	gs->ID = ID;

	return gs;
}

/**
 * Register previously initialised button with a game state
 * @param gs		gamestate structure to associate with
 * @param button	button structure to be associated
 * @param layer		render layer
 */
void gamestate_registerButton(gamestate_t* gs, button_t* button, int layer) {

	if (gs->button_pop < MAX_BUTTONS) {
		gs->buttonPool[gs->button_pop] = button;
		gs->buttonLayers[gs->button_pop] = layer;
		gs->button_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register button with gamestate ( %d / %d ) \n", gs->button_pop, MAX_BUTTONS);
	}
}

/**
 * Register previously initialised image texture with a game state
 * @param gs		gamestate structure to associate with
 * @param tex		image texture to be associated
 * @param layer		render layer
 */
void gamestate_registerTexture(gamestate_t* gs, texture_t* tex, int layer) {

	if (gs->texture_pop < MAX_TEXTURES) {
		gs->texturePool[gs->texture_pop] = tex;
		gs->textureLayers[gs->texture_pop] = layer;
		gs->texture_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register image texture with gamestate ( %d / %d ) \n", gs->texture_pop, MAX_TEXTURES);
	}
}

/**
 * Register previously initialised sound effect with game state
 * @param gs		gamestate structure to associate with
 * @param sd		sound to be assosicated
 */
void gamestate_registerSound(gamestate_t* gs, sound_t* sd) {

	if (gs->sound_pop < MAX_SOUNDS) {
		gs->soundPool[gs->sound_pop] = sd;
		gs->sound_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register sound with gamestate ( %d / %d ) \n", gs->sound_pop, MAX_SOUNDS);
	}
}

/**
 * Register previously initialised pixelarray with game state
 * The pixelarray will then be automatically handled.
 * @param gs		gamestate structure to associate with
 * @param pa		pixel array to be associated
 * @param layer		render layer
 */
void gamestate_registerPixelarray(gamestate_t* gs, pixelarray_t* pa, int layer) {

	if (gs->pixelarray_pop < MAX_PIXELARRAYS) {
		gs->pixelarrayPool[gs->pixelarray_pop] = pa;
		gs->pixelarrayLayers[gs->pixelarray_pop] = layer;
		gs->pixelarray_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register pixel array with gamestate ( %d of %d ) \n", gs->pixelarray_pop, MAX_PIXELARRAYS);
	}
}

/**
 * Register previously initialised rectangle structure with game state
 * @param gs		gamestate structure to associate with
 * @param rect		rectangle to be associated
 * @param layer		render layer
 */
void gamestate_registerRectangle(gamestate_t* gs, rectangle_t* rect, int layer) {

	if (gs->rectangle_pop < MAX_RECTANGLES) {
		gs->rectanglePool[gs->rectangle_pop] = rect;
		gs->rectangleLayers[gs->rectangle_pop] = layer;
		gs->rectangle_pop += 1;
	} else {
		printf(PRT_WARN"Cannot register rectangle with gamestate ( %d / %d ) \n", gs->rectangle_pop, MAX_RECTANGLES);
	}
}


/**
 * Update children of gamestate
 * @param gs		gamestate structure concerned
 * @param mouseX	mouse x-coordinate
 * @param mouseY	mouse y-coordinate
 * @param mouse1 	mouse button1 state
 */
void gamestate_update(gamestate_t* gs) {

	if (gs == NULL) return;

	// update buttons
	for (int i = 0; i < gs->button_pop; i++) {
		if (gs->buttonPool[i] != NULL) {
			button_update(gs->buttonPool[i], mouseX, mouseY, mouse1);
			if (button_isClicked(gs->buttonPool[i])) sound_play(buttonpress_sound, 0);
			if (button_isNewHover(gs->buttonPool[i])) sound_play(buttonhover_sound, 0);
		}
	}

	// Call gs-specific update function

	// Probably need to update other stuff


}

/**
 * Render children of gamestate
 * @param gs		gamestate structure concerned
 */
void gamestate_render(gamestate_t* gs) {

	// Registering an object requires assigning a 'layer' to the object
	// Lower layer = rendered earlier = appears below objects on higher layers
	// Objects with shared layers will render based on when they are registered and the type of object
	// It should be possible to offer the capability to 'swap' the order

	// Render with layer-based priority
	bool allRendered = false;
	int  buttons_rendered = 0;
	int  textures_rendered = 0;
	int  pixelarrays_rendered = 0;
	int  rectangles_rendered = 0;

	while (!allRendered){

		// Loop through layers from furthest to nearest
		for(int L = 0; L < MAX_LAYERS; L++){

			// render image textures on this layer
			for (int i = 0; i < gs->texture_pop; i++) {
				if (gs->textureLayers[i] == L) {
					texture_render(gs->texturePool[i]);
					textures_rendered++;
				}
			}

			// render pixel arrays on this layer
			for (int i = 0; i < gs->pixelarray_pop; i++) {
				if (gs->pixelarrayLayers[i] == L) {
					pixelarray_render(gs->pixelarrayPool[i]);
					pixelarrays_rendered++;
				}
			}

			// render buttons
			for (int i = 0; i < gs->button_pop; i++) {
				if (gs->buttonLayers[i] == L) {
					button_render(gs->buttonPool[i]);
					buttons_rendered++;
				}
			}

			// render rectangles
			for (int i = 0; i < gs->rectangle_pop; i++) {
				if (gs->rectangleLayers[i] == L) {
					rectangle_render(gs->rectanglePool[i]);
					rectangles_rendered++;
				}
			}

		}

		// all done?
		allRendered =  (buttons_rendered 		== gs->button_pop)
					&& (textures_rendered 		== gs->texture_pop)
					&& (pixelarrays_rendered	== gs->pixelarray_pop)
					&& (rectangles_rendered		== gs->rectangle_pop);

	}

}

/**
 * Free memory being used by gamestate structure and its children
 * @param gs	gamestate structure to free
 */
void gamestate_free(int ID) {

	gamestate_t* gs = gamestate_accountant[ID];

	gamestate_accountant[gs->ID] = 0;

	// free buttons
	for (int i = 0; i < gs->button_pop; i++) {
		if (gs->buttonPool[i] != NULL) {
			button_free(gs->buttonPool[i]);
		}
	}

	// free image textures
	for (int i = 0; i < gs->texture_pop; i++) {
		if (gs->texturePool[i] != NULL) {
			texture_free(gs->texturePool[i]);
		}
	}

	// free sound effects
	for (int i = 0; i < gs->sound_pop; i++) {
		if (gs->soundPool[i] != NULL) {
			sound_free(gs->soundPool[i]);
		}
	}

	// free rectangles
	for (int i = 0; i < gs->rectangle_pop; i++) {
		if (gs->rectanglePool[i] != NULL) {
			rectangle_free(gs->rectanglePool[i]);
		}
	}

	// free pixelarrays
	for (int i = 0; i < gs->pixelarray_pop; i++) {
		if (gs->pixelarrayPool[i] != NULL) {
			pixelarray_free(gs->pixelarrayPool[i]);
		}
	}

	printf(PRT_MSG"Freeing gamestate %d \n", gs->ID);
	free(gs);
}

/**
 * Return pointer to current gamestate
 */
gamestate_t* gamestate_getCurrent() {
	if (_gamestate_current <= _STATE_START || _gamestate_current >= _STATE_END) {
		printf(PRT_ERROR"Current gamestate ID = %d; cannot return it - out of range \n", _gamestate_current);
		exit(EXIT_FAILURE);
	}
	return gamestate_accountant[_gamestate_current];
}

/**
 * Return pointer to previous gamestate
 */
gamestate_t* gamestate_getPrevious() {
	if (_gamestate_previous != -1) {
		return gamestate_accountant[_gamestate_previous];
	} else {
		printf(PRT_WARN"There was no previous gamestate, so it cannot be returned \n");
		return NULL;
	}
}

/**
 * Return pointer to next gamestate
 */
gamestate_t* gamestate_getNext() {
	if (_gamestate_next != -1) {
		return gamestate_accountant[_gamestate_next];
	} else {
		printf(PRT_WARN"There is no next gamestate, so it cannot be returned \n");
		return NULL;
	}
}

/**
 * Note: In most instances, call setNext() instead as it is safer.
 * Set current gamestate based on ID (from enum). This function changes the gamestate immediately.
 * @param ID	ID of new gamestate
 */
void gamestate_makeCurrent(int ID) {
	if (ID <= _STATE_START || ID >= _STATE_END) {
		printf(PRT_WARN"Cannot set gamestate to %d (undefined state) \n", ID);
	} else {
		if (gamestate_accountant[ID] == NULL) {
			printf(PRT_WARN"Cannot set gamestate to %d because it is not initialised \n", ID);
		} else if (!gamestate_isReady(ID)){
			printf(PRT_WARN"Cannot set gamestate to %d because it is not ready \n ", ID);
		} else {
			printf(PRT_MSG"Setting current gamestate to %d \n", ID);
			_gamestate_previous = _gamestate_current;
			_gamestate_current = ID;
		}
	}
}


/**
 * Set the new gamestate based on ID (from enum). Waits 1 update before changing gamestate.
 * @param ID	ID of next gamestate
 */
void gamestate_setNext(int ID) {
	if (ID <= _STATE_START || ID >= _STATE_END) {
		printf(PRT_WARN"Cannot set next gamestate to %d (undefined state) \n", ID);
	} else {
		if (gamestate_accountant[ID] == NULL) {
			printf(PRT_WARN"Cannot set next gamestate to %d because it is not initialised \n", ID);
		} else if (!gamestate_isReady(ID)){
			printf(PRT_WARN"Cannot set next gamestate to %d because it is not ready \n ", ID);
		} else {
			printf(PRT_MSG"Setting next gamestate to %d \n", ID);
			_gamestate_next = ID;
		}
	}
}


/**
 * Flag a gamestate as ready for being updated/rendered
 * @param ID	ID of gamestate
 */
void gamestate_setReady(int ID) {
	if (ID <= _STATE_START || ID >= _STATE_END) {
		printf(PRT_WARN"Cannot ready gamestate to %d (undefined state) \n", ID);
	} else {
		if (gamestate_accountant[ID] == NULL) {
			printf(PRT_WARN"Cannot ready gamestate %d because it is not initialised \n", ID);
		} else if (gamestate_isReady(ID)){
			printf(PRT_WARN"Cannot ready gamestate to %d, because it already was so \n ", ID);
		} else {
			printf(PRT_MSG"Gamestate %d made ready \n", ID);
			gamestate_accountant[ID]->_ready = true;
		}
	}
}

/**
 * Check readiness of gamestate, returning it as a boolean
 * @param ID	ID of gamestate
 */
bool gamestate_isReady(int ID) {
	if (ID <= _STATE_START || ID >= _STATE_END) {
		printf(PRT_WARN"Cannot retrieve readiness of gamestate %d (undefined state) \n", ID);
	} else {
		if (gamestate_accountant[ID] == NULL) {
			printf(PRT_WARN"Cannot retrieve readiness of gamestate %d because it is not initialised \n", ID);
		} else {
			return gamestate_accountant[ID]->_ready;
		}
	}
	return false;
}

/**
 * Reset gamestate to initial conditions
 * @param ID	ID of gamestate
 */
void gamestate_reset(int ID) {
	gamestate_free(ID);
	gamestate_init(ID);
}