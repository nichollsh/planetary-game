#include <planetary.h>

/**
 * A rectangle structure provides a more robust way of drawing rectangles.
 * It allows storage of geometry without needing 'external' variables.
 * Functions are also provided to intuitively modify the rectangles.
 * This file (primitive_rectangle.c) still contains a function at the bottom
 * for directly drawing a rectangle without using a 'rectangle_t' structure.
 */


/**
 * Initialise rectangle structure, allocating memory, and setting variables.
 * @param gRenderer SDL Renderer to use
 * @param x			x coord of rectangle
 * @param y			y coord of rectangle
 * @param w			width of rectangle
 * @param h			height of rectangle
 * @param color		color of rectangle (RGBA)
 */
rectangle_t* rectangle_init(SDL_Renderer* gRenderer, float x, float y, float w, float h, SDL_Color color){

	rectangle_t* rect = (rectangle_t*) malloc(sizeof(rectangle_t));
	if (rect == NULL) {
		printf(PRT_ERROR"Could not allocate memory for rectangle \n");
		exit(EXIT_FAILURE);
	}

	rect->x = x;
	rect->y = y;
	rect->w = w;
	rect->h = h;
	rect->color 	= color;
	rect->filled 	= true;
	rect->hide		= false;
	rect->gRenderer = gRenderer;

	rect->r.x 		= (int) roundf(x);
	rect->r.y 		= (int) roundf(y);
	rect->r.w		= (int) roundf(w);
	rect->r.h 		= (int) roundf(h);
	
	return rect;
}

/**
 * Free memory used by rectangle structure
 * @param rect Structure to free
 */
void rectangle_free(rectangle_t* rect) {
	free(rect);
}

/**
 * Render an initialised rectangle sturcture
 * @param rect Structure to render
 */
void rectangle_render(rectangle_t* rect) {
	if (rect->hide) return;

	SDL_SetRenderDrawColor(rect->gRenderer, rect->color.r, rect->color.g, rect->color.b, rect->color.a);
	
	if (rect->filled) {
		SDL_RenderFillRect(rect->gRenderer, &rect->r);
	} else {
		SDL_RenderDrawRect(rect->gRenderer, &rect->r);
	}
}


void rectangle_setX(rectangle_t* rect, float x) {
	rect->r.x = (int) roundf(x);
	rect->x = x;
}

void rectangle_setW(rectangle_t* rect, float w) {
	rect->r.w = (int) roundf(w);
	rect->w = w;
}

void rectangle_setY(rectangle_t* rect, float y) {
	rect->r.y = (int) roundf(y);
	rect->y = y;
}

void rectangle_setH(rectangle_t* rect, float h) {
	rect->r.h = (int) roundf(h);
	rect->h = h;
}

float rectangle_getX(rectangle_t* rect) {
	return rect->x;
}

float rectangle_getW(rectangle_t* rect) {
	return rect->w;
}

float rectangle_getY(rectangle_t* rect) {
	return rect->y;
}

float rectangle_getH(rectangle_t* rect) {
	return rect->h;
}

void rectangle_toggleHide(rectangle_t* rect) {
	rect->hide = !rect->hide;
}

/**
 * Directly draws rectangle using SDL, this ignores the 'rectangle_t' structure
 * @param gRenderer		Renderer to use
 * @param x				x position
 * @param y				y position
 * @param w				width of rectangle
 * @param h				height of rectangle
 * @param isFilled		filled or outline? (true/false)
 */
void drawSDLRect(SDL_Renderer* gRenderer, float x, float y, float w, float h, bool isFilled) {

	SDL_Rect rectangular;
	rectangular.x = (int) roundf(x);
	rectangular.y = (int) roundf(y);
	rectangular.w = (int) roundf(w);
	rectangular.h = (int) roundf(h);

	if (isFilled) {
		SDL_RenderFillRect(gRenderer, &rectangular);
	} else {
		SDL_RenderDrawRect(gRenderer, &rectangular);
	}
	
}

