#include "planetary.h"

/**
 * Sound effects are short audio files that are played when an event occurs.
 * They are not music, but things like button presses, or notification alerts.
 * More than one sound effect can be played at once, unlike music.
 * A sound structure stores a single sound effect.
 */

/**
 * Load a sound effect from the filesystem, and return a sound structure
 * @param file		location of file containing sound effect in file system
 */
sound_t* sound_loadFromFile(char* file) {

	sound_t* sd = (sound_t*) malloc(sizeof(sound_t));
	if (sd == NULL) {
		printf(PRT_ERROR"Could not allocate memory for sound! \n");
		exit(EXIT_FAILURE);
	}

	sd->chunk = Mix_LoadWAV(file);
	if( sd->chunk == NULL ){
		printf(PRT_ERROR"Failed to load sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	Mix_VolumeChunk(sd->chunk, (int) MIX_MAX_VOLUME);
	sd->channel = -1337;

	return sd;
}

/**
 * Free memory being used by sound structure and its children
 * @param sd	sound structure to free
 */
void sound_free(sound_t* sd) {
	Mix_FreeChunk(sd->chunk);
	free(sd);
}

/**
 * Play sound effect
 * @param sd			sound structure containing effect to play
 * @param loopNumber	number of times to loop effect (-1 => infinite, 0 => don't loop, 1 => loop once, etc)
 */
void sound_play(sound_t* sd, int loopNumber) {
	if(sd->channel != -1337 && SOUND_RESET == true) Mix_HaltChannel(sd->channel);
	sd->channel = Mix_PlayChannel(-1, sd->chunk,loopNumber);
}

/**
 * Set volume of sound effect
 * @param sd			sound structure containing effect
 * @param vol			new volume [0.0 -- 1.0]
 */
void sound_setVolume(sound_t* sd, float vol) {
	Mix_VolumeChunk(sd->chunk, vol*MIX_MAX_VOLUME);
}