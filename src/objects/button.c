#include "planetary.h"
#include "globalvars.h"


button_t* button_init(SDL_Renderer* gRenderer,  int x, int y, char* text, TTF_Font* gFont){

	button_t* bt = (button_t*) malloc(sizeof(button_t));
	if (bt == NULL) {
		printf(PRT_ERROR"Could not allocate memory for button! \n");
		exit(EXIT_FAILURE);
	}

	// Collect parameters
	bt->x = x;
	bt->y = y;
	bt->b = 2;		// Set to a default value; can be changed manually if required
	bt->w = 140;
	bt->h = 35;
	bt->_held = false;
	bt->_hovered = false;
	bt->_clicked = false;
	bt->_released = true;
	bt->_newhover = false;
	bt->gRenderer = gRenderer;

	bt->gFont = gFont;
	bt->borderColor = color_black;
	bt->borderColorAlt = color_bluewhite;
	bt->foregroundColor = color_black;
	bt->backgroundColor = color_bluewhite;
	bt->backgroundColorAlt = color_bluegrey;

	if ( bt->backgroundColor.a != 0xFF ) {
		printf(PRT_WARN"Will ignore alpha channel of background colour (button) \n");
	}
	if ( bt->borderColor.a != 0xFF ) {
		printf(PRT_WARN"Will ignore alpha channel of border colour (button) \n");
	}

	if (strlen(text) < STRLEN) {
		strcpy(bt->text, text);
	} else {
		printf(PRT_WARN"Button text too long \n");
		strcpy(bt->text, "Orig. text too long!");
	}

	bt->texture = texture_loadFromText(gRenderer,bt->text, bt->gFont, bt->foregroundColor);

	bt->px = (bt->w - 2*bt->b - bt->texture->pix_width) /2;
	bt->py = (bt->h - 2*bt->b - bt->texture->pix_height)/2;

	texture_setX(bt->texture, bt->x + bt->b + bt->px);
	texture_setY(bt->texture, bt->y + bt->b + bt->py);

	return bt;
}

void button_update(button_t* bt, int mouseX, int mouseY, int mouseState){
	if ((mouseX > bt->x) && (mouseX < bt->x + bt->w) && (mouseY > bt->y) && (mouseY < bt->y+bt->h)) {
		if (bt->_hovered == false) {
			bt->_newhover = true;
		} else {
			bt->_newhover = false;
		}
		bt->_hovered = true;
	} else {
		bt->_hovered = false;
		bt->_held = false;
		bt->_newhover = false;
	}

	bt->_clicked = false;

	if (bt->_hovered) {
		if (mouseState == 1) {
			bt->_held = true;
			if (bt->_released == true) {
				bt->_clicked  = true;
				bt->_released = false;
			}
		} else {
			bt->_held = false;
			bt->_released = true;
		} 
	}


}

void button_render(button_t* bt){
	// Outline
	if (bt->_held) {
		SDL_SetRenderDrawColor(bt->gRenderer, bt->borderColorAlt.r, bt->borderColorAlt.g, bt->borderColorAlt.b, 0xFF);
	} else {
		SDL_SetRenderDrawColor(bt->gRenderer, bt->borderColor.r, bt->borderColor.g, bt->borderColor.b, 0xFF);
	}

	drawSDLRect(bt->gRenderer, bt->x , bt->y , bt->w, bt->h, true );

	// Background
	if (bt->_hovered) {
		SDL_SetRenderDrawColor(bt->gRenderer, bt->backgroundColorAlt.r, bt->backgroundColorAlt.g, bt->backgroundColorAlt.b, 0xFF);
	} else {
		SDL_SetRenderDrawColor(bt->gRenderer, bt->backgroundColor.r, bt->backgroundColor.g, bt->backgroundColor.b, 0xFF);
	}

	
	drawSDLRect(bt->gRenderer, bt->x + bt->b, bt->y + bt->b, bt->w - bt->b*2, bt->h - bt->b*2, true);

	// Contents
	texture_render(bt->texture);
}

void button_setText(button_t* bt, char* newText) {
	strcpy(bt->text, newText);
	bt->texture = texture_loadFromText(gRenderer,bt->text, bt->gFont, bt->foregroundColor);

	bt->px = (bt->w - 2*bt->b - bt->texture->pix_width) /2;
	bt->py = (bt->h - 2*bt->b - bt->texture->pix_height)/2;

	texture_setX(bt->texture, bt->x + bt->b + bt->px);
	texture_setY(bt->texture, bt->y + bt->b + bt->py);
}

void button_free(button_t* bt){
	texture_free(bt->texture);
	free(bt);

}

bool button_isHeld(button_t* bt) {
	return bt->_held;
}

bool button_isHovered(button_t* bt) {
	return bt->_hovered;
}

bool button_isClicked(button_t* bt) {
	return bt->_clicked;
}

bool button_isNewHover(button_t* bt) {
	return bt->_newhover;
}