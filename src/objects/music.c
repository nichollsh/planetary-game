#include "planetary.h"

/**
 * Music objects store a piece of music to be played.
 * It is a limitation of SDL that only 1 music object can be played at a time.
 * This file (music.c) provides different functions, for the abstraction of SDL music,
 * and for ease of use.
 */


/**
 * Load music from filesystem into music structure. Returns music stucture.
 * @param path		Path to location of file containing music (in filesystem)
 */
music_t* music_loadFromFile(char* file) {

	music_t* ms = (music_t*) malloc(sizeof(music_t));
	if( ms == NULL ) {
		printf(PRT_ERROR"Could not allocate memory for music\n");
		exit(EXIT_FAILURE);
	}


	ms->music = Mix_LoadMUS(file);
	if( ms->music == NULL ) {
		printf(PRT_ERROR"Failed to load music! SDL_mixer Error: %s\n", Mix_GetError());
		exit(EXIT_FAILURE);
	}

	return ms;	
}

/**
 * Free memory being used by music structure and its children
 * @param ms	music structure to free
 */
void music_free(music_t* ms) {
	Mix_FreeMusic(ms->music);
	free(ms);
}

/**
 * Play music contained by music structure. Does not stop currently playing music.
 * @param ms			music structure to play from
 * @param loopnumber	number of times to loop music (-1 => infinite, 0 => don't loop, 1 => loop once, etc)
 */
void music_start(music_t* ms, int loopNumber) {
	if (Mix_PlayingMusic() == 0) { // if music not currently playing
		Mix_PlayMusic(ms->music, loopNumber);
	} else {
		printf(PRT_WARN"Cannot start new music, since music is currently playing\n");
	}
}


/**
 * Play music contained by music structure. Stops current music.
 * @param ms			music structure to play from
 * @param loopnumber	number of times to loop music (-1 => infinite, 0 => don't loop, 1 => loop once, etc)
 */
void music_startf(music_t* ms, int loopNumber) {
	if (Mix_PlayingMusic() == 0) { // if music not currently playing
		Mix_PlayMusic(ms->music, loopNumber);
	} else {
		Mix_HaltMusic();
		Mix_PlayMusic(ms->music, loopNumber);
	}
}


/**
 * Toggle currently playing music between play and pause
 */
void music_toggle() {
	if (Mix_PausedMusic() == 1) {
		Mix_ResumeMusic();
	} else if (Mix_PlayingMusic() == 1) {
		Mix_PauseMusic();
	} else {
		printf(PRT_WARN"Music is nether paused, nor playing! \n");
	}
}

/**
 * Stops the currently playing music
 */
void music_stop() {
	if (Mix_PlayingMusic() == 1) {
		Mix_HaltMusic();
	} else {
		printf(PRT_WARN"Cannot stop music, because none is playing \n");
	}
}