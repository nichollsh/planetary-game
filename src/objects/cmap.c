#include "planetary.h"
#include "cmap_schemes.h"


/**
 * Colormaps are used for mapping data values to color values.
 * Colormaps have a 'values' array and an 'rgba_colors' array of the same length.
 * At each index, the value in the 'values' array corresponds to that in the 'colors' array.
 * This file (cmap.c) provides functions for mapping data sets using these arrays, and interpolating
 * between the stored colors. It imitates the library from matplotlib, and was inspired by it.
 */


/**
 * Initialise color map
 * @param scheme		color scheme to use (from cmap_schemes enum)
 * @param reversed		boolean value used to flip cmap direction (true => reversed)
 */
cmap_t* cmap_init(int scheme, bool reversed) {
	// 0x AB CD EF GH \0
	// 01 23 45 67 89 10
	//    Al Re Gr Bl
	
	cmap_t* cm = (cmap_t*) malloc(sizeof(cmap_t));
	if (cm == NULL) {
		printf(PRT_ERROR"Could not allocate memory for ColorMap! \n");
		exit(EXIT_FAILURE);
	}

	cm->sample_size		= SAMPLE_SIZE;
	cm->_format			= SDL_PIXELFORMAT_ARGB8888;
	cm->interpolation	= CMAP_INTERP_NONE;
	cm->reversed 		= reversed;

	cm->rgba_colors = (SDL_Color*) malloc(cm->sample_size * sizeof(SDL_Color));
	if (cm->rgba_colors == NULL) {
		printf(PRT_ERROR"Could not allocate memory for ColorMap colors! \n");
		exit(EXIT_FAILURE);
	}

	if (!reversed) {
		cm->values = linspace(0.0, 1.0, SAMPLE_SIZE);
	} else {
		cm->values = linspace(1.0, 0.0, SAMPLE_SIZE);
	}


	Uint8 colors[SAMPLE_SIZE][4];
	if (scheme > _SCHEME_START && scheme < _SCHEME_END) {
		switch (scheme) {
			case CMAP_SCHEME_GREYS:
			memcpy(colors, colorsGreys, sizeof(colorsGreys));
			break;

			case CMAP_SCHEME_MARS:
			memcpy(colors, colorsMars, sizeof(colorsMars));
			break;

			case CMAP_SCHEME_OCEAN:
			memcpy(colors, colorsOcean, sizeof(colorsOcean));
			break;
			
			case CMAP_SCHEME_COOLWARM:
			memcpy(colors, colorsCoolWarm, sizeof(colorsCoolWarm));
			break;

			case CMAP_SCHEME_TERRAIN:
			memcpy(colors, colorsTerrain, sizeof(colorsTerrain));
			break;

			case CMAP_SCHEME_EARTH:
			memcpy(colors, colorsEarth, sizeof(colorsEarth));
			break;

			default:
			printf(PRT_WARN"Unknown ColorMap color scheme \n");
			memcpy(colors, colorsGreys, sizeof(colorsGreys));
			break;
		}
	}

	// For each of the provided colours
	for (int i = 0; i < cm->sample_size ; i++){ 
		// Alpha
		cm->rgba_colors[i].a = colors[i][0];

		// Red
		cm->rgba_colors[i].r = colors[i][1];

		// Green
		cm->rgba_colors[i].g = colors[i][2];

		// Blue
		cm->rgba_colors[i].b = colors[i][3];
	}

	return cm;
	
}

/**
 * Set interpolation method to be used when getting colours
 * @param cm			colormap structure
 * @param interp		interpolation type from 'cmap_interp' enum
 */
void cmap_setInterpolation(cmap_t* cm, int interp) {
	if ( interp <= _INTERP_START || interp >= _INTERP_END) {
		printf(PRT_WARN"Invalid interpolation type! \n");
	} else {
		if (cm->sample_size <= 3) {
			// Can't use average or linear
			if ( interp == CMAP_INTERP_AVERAGE || interp == CMAP_INTERP_LINEAR) {
				interp = CMAP_INTERP_NEAREST;
				printf(PRT_WARN"Not enough samples in ColorMap to use higher order interpolation methods \n");
			} 
		} else
		cm->interpolation = interp;
	}
}

/**
 * Get color corresponding to value using a colormap
 * @param cm			colormap structure
 * @param value			value to get the color for
 */
pix_t cmap_getColor(cmap_t* cm, float value){
	Uint8 a = 0xFF;
	Uint8 r = 0xFF;
	Uint8 g = 0x00;
	Uint8 b = 0xFF;

	if (value < 0.0 || value > 1.0) {
		printf(PRT_WARN"ColorMap lookup value is out of bounds ( value = %g ) \n", value);
		return 0x00000000;
	}

	if (cm->interpolation == CMAP_INTERP_NONE) {
		// If the value provided doesn't exactly map to a value in the array, then the result is transparent
		for (int i = 0; i < cm->sample_size; i++) {
			if (value == cm->values[i]) {
				a = cm->rgba_colors[i].a;
				r = cm->rgba_colors[i].r;
				g = cm->rgba_colors[i].g;
				b = cm->rgba_colors[i].b;
				break;
			} else {
				a = 0x00;
			}
		}

	} else if (cm->interpolation == CMAP_INTERP_NEAREST) {
		// Find nearest assigned value in color map, and return its color
		float dx = 1.0; // lowest difference between lookup and assigned
		int   ni = -1;  // index which gives current dx

		for (int i = 0; i < cm->sample_size; i++) {
			float test_dx = fabsf( value - cm->values[i] );
			if (test_dx < dx) {
				dx = test_dx;
				ni = i;
			}
		}
		
		if (ni != -1) {
			a = cm->rgba_colors[ni].a;
			r = cm->rgba_colors[ni].r;
			g = cm->rgba_colors[ni].g;
			b = cm->rgba_colors[ni].b;
		} else {
			a = 0x00;
			printf(PRT_WARN"Error interpolating value '%g' to cmap (using CMAP_INTERP_NEAREST) \n", value);
		}

	} else if (cm->interpolation == CMAP_INTERP_AVERAGE) {
		// Find nearest two values and average their colors
		float dx0 = 1.0; // lowest difference between lookup and assigned
		int   ni0 = -1;  // index which gives current dx0
		

		for (int i = 0; i < cm->sample_size; i++) {
			float test_dx = fabsf( value - cm->values[i] );
			if (test_dx < dx0) {
				dx0 = test_dx;
				ni0 = i;
			}
		}

		dx0 = 1.0;
		int   ni1 = -1;

		for (int i = 0; i < cm->sample_size; i++) {
			if (i == ni0) continue;
			float test_dx = fabsf( value - cm->values[i] );
			if (test_dx < dx0) {
				dx0 = test_dx;
				ni1 = i;
			}
		}

		if (ni0 != -1 && ni1 != -1) {
			a = (cm->rgba_colors[ni0].a + cm->rgba_colors[ni1].a)/2;
			r = (cm->rgba_colors[ni0].r + cm->rgba_colors[ni1].r)/2;
			g = (cm->rgba_colors[ni0].g + cm->rgba_colors[ni1].g)/2;
			b = (cm->rgba_colors[ni0].b + cm->rgba_colors[ni1].b)/2;
		} else {
			a = 0x00;
			printf(PRT_WARN"Error interpolating value '%g' to cmap (using CMAP_INTERP_AVERAGE) [%d, %d]\n", value, ni0, ni1);
		}

		
	} else if (cm->interpolation == CMAP_INTERP_LINEAR) {
		// Find nearest two values and linearly interpolate between their colours to 
		// estimate correct the colour for lookup 
		
		float dx0 = 1.0; // lowest difference between lookup and assigned
		int   ni0 = -1;  // index which gives current dx0
		

		for (int i = 0; i < cm->sample_size; i++) {
			float test_dx = fabsf( value - cm->values[i] );
			if (test_dx < dx0) {
				dx0 = test_dx;
				ni0 = i;
			}
		}

		dx0 = 1.0;
		int   ni1 = -1;

		for (int i = 0; i < cm->sample_size; i++) {
			if (i == ni0) continue;
			float test_dx = fabsf( value - cm->values[i] );
			if (test_dx < dx0) {
				dx0 = test_dx;
				ni1 = i;
			}
		}

		if (ni0 != -1 && ni1 != -1) {

			float dist = value - cm->values[ni0];			//  x - x0
			float dnom = cm->values[ni1] - cm->values[ni0]; // x1 - x0

			a = cm->rgba_colors[ni0].a + dist * (cm->rgba_colors[ni1].a - cm->rgba_colors[ni0].a) / dnom;
			r = cm->rgba_colors[ni0].r + dist * (cm->rgba_colors[ni1].r - cm->rgba_colors[ni0].r) / dnom;
			g = cm->rgba_colors[ni0].g + dist * (cm->rgba_colors[ni1].g - cm->rgba_colors[ni0].g) / dnom;
			b = cm->rgba_colors[ni0].b + dist * (cm->rgba_colors[ni1].b - cm->rgba_colors[ni0].b) / dnom;

		} else {
			a = 0x00;
			printf(PRT_WARN"Error interpolating value '%g' to cmap (using CMAP_INTERP_LINEAR) [%d, %d]\n", value, ni0, ni1);
		}
		
	}

	pix_t out = 0;
	int bitshift = 8;

	out = a;
	out = (out << bitshift) + r;
	out = (out << bitshift) + g;
	out = (out << bitshift) + b;	

	return out;
}

/**
 * Perform value->color conversion over an entire 1D array of values
 * @param cm			colormap structure
 * @param len			length of array
 * @param array			array of values to convert
 */
pix_t* cmap_getColor_1D(cmap_t* cm, int len, float array[len]) {
	pix_t* out = (pix_t*) calloc(len, sizeof(pix_t));

	if (out == NULL) {
		printf(PRT_ERROR"Could not allocate memory for cmap pixels! \n");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < len; i++) {
		out[i] = cmap_getColor(cm, array[i]);
	}

	return out;
}

/**
 * Perform weighted average of two pixels
 * @param a				Pixel A
 * @param b				Pixel B
 * @param ratio			Ratio to average them by. New = ratio*A + (1-ratio)*B.
 */
pix_t cmap_combinePixels0D(pix_t a, pix_t b,float ratio){

	Uint8 a_A = a >> 24;
	Uint8 a_R = a >> 16;
	Uint8 a_G = a >> 8;
	Uint8 a_B = a >> 0;

	Uint8 b_A = b >> 24;
	Uint8 b_R = b >> 16;
	Uint8 b_G = b >> 8;
	Uint8 b_B = b >> 0;

	float rp = 1 - ratio;

	Uint8 c_A = (Uint8) ( ratio*a_A + rp*b_A );
	Uint8 c_R = (Uint8) ( ratio*a_R + rp*b_R );
	Uint8 c_G = (Uint8) ( ratio*a_G + rp*b_G );
	Uint8 c_B = (Uint8) ( ratio*a_B + rp*b_B );

	pix_t c = 0;

	c = c_A;
	c = (c << 8) + c_R;
	c = (c << 8) + c_G;
	c = (c << 8) + c_B;

	return c;

}

/**
 * Free memory being used by color map
 * @param cm			colormap structure
 */
void cmap_free(cmap_t* cm) {
	free(cm->rgba_colors);
	free(cm->values);
	free(cm);
}
