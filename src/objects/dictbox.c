#include "planetary.h"

/**
 * A dictionary box ('dictbox') is used to display keys and values, like a Python dictionary stores them.
 * You can add as many keys and values as required; the number of such pairs is the number of 'lines'.
 * They are rendered by this code as a list, to the screen.
 * Designed with the idea of a debug overlay in mind.
 */


static int		defaultInt = 1337;
static char*	defaultStr = "default ";

/**
 * Initialises dictbox object. Assign pointers to elements of 'values' and 'labels', for them to display values held in variables. Assign pointers using dictbox_setLabel and dictbox_setValue
 * @param lines				number of lines in dictbox
 * @param fixedW			width of dictbox in pixels, if to be a fixed size (set to -1 to tight fit around contents)
 * @param gFont				font to render text in
 * @param borderColor		color of border
 * @param foregroundColor	color of text
 * @param backgroundColor	color of background
 */
dictbox_t* dictbox_init(int lines, int fixedW, int x, int y, TTF_Font* gFont, SDL_Color borderColor, SDL_Color foregroundColor, SDL_Color backgroundColor) { // dictbox, lines

	dictbox_t* db = (dictbox_t*) malloc(sizeof(dictbox_t));

	if (db == NULL) {
		printf("Error allocating memory for dictbox! \n");
		exit(EXIT_FAILURE);
	}

	texture_t** p =  (texture_t**) malloc(lines * sizeof(texture_t*));
	if (p == NULL) {
		printf("Error allocating memory for dictbox textures \n");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < lines ; i++){
		p[i] = (texture_t*)  calloc(lines * sizeof(texture_t),sizeof(texture_t));
		if (p[i] == NULL) {
			printf("Error allocating memory for dictbox texture %d \n", i);
			exit(EXIT_FAILURE);
		}
	}
	db->textures = p;

	db->w = fixedW; // set to -1 if dynamic

	// Default values
	db->x = x;
	db->y = y;
	db->b = 1;
	db->p = 1;
	db->s = 1;
	db->lines = lines;
	db->gFont = gFont;
	db->borderColor = borderColor;
	db->foregroundColor = foregroundColor;
	db->backgroundColor = backgroundColor;
	db->current_pop = 0;

	// Set these memory addresses - to be read from by update function

	db->labels = (char**) malloc(lines *  STRLEN * sizeof(char));
	db->values = (int**) malloc(lines *  sizeof(int*));

	for (int i = 0; i < lines; i++) {
		db->labels[i] = defaultStr;
		db->values[i] = &defaultInt;
	}

	return db;
}

/**
 * Updates and renders dictbox object. Updates values to be displayed, and renders to screen.
 * @param gRenderer	Renderer to render dictbox to
 * @param db		Textbox structure
 */
void dictbox_update(SDL_Renderer* gRenderer, dictbox_t* db) {

	// Render each label as text
	char text[STRLEN];
	char buffer[STRLEN];
	
	for (int i = 0; i < db->lines; i++) {

		memset(text,0,STRLEN * sizeof(char));
		memset(buffer,0,STRLEN * sizeof(char));

		strcat(text, db->labels[i]);
		strcat(text, ": ");

		sprintf(buffer, "%d", *(db->values[i]));				
		strcat(text,buffer);

		texture_free(db->textures[i]);
		db->textures[i] = texture_loadFromText(gRenderer, text, db->gFont, db->foregroundColor);

		// texture_updateText(db->textures[i], text, db->gFont, db->foregroundColor);

		
	}

	// Dynamic params
	int debugW = 0;
	int debugH = 0;
	for (int i = 0; i < db->lines; i++) {
		if (db->w < 0) {
			if (texture_getWidth(db->textures[i]) > debugW) {
				debugW = texture_getWidth(db->textures[i]);
			}
		} else {
			debugW = db->w;
		}

		debugH += texture_getHeight(db->textures[i]) + db->s;
	}
	debugW += 2 * (db->b + db->p);
	debugH += 2 * (db->b + db->p);

	// Outline
	SDL_SetRenderDrawColor(gRenderer, db->borderColor.r, db->borderColor.g, db->borderColor.b, db->borderColor.a);
	drawSDLRect(gRenderer, db->x , db->y , debugW, debugH, false );

	// Background
	SDL_SetRenderDrawColor(gRenderer, db->backgroundColor.r, db->backgroundColor.g, db->backgroundColor.b, db->backgroundColor.a);
	drawSDLRect(gRenderer, db->x + db->b, db->y + db->b, debugW - db->b*2, debugH - db->b*2, true);

	// Contents
	int offset = 0; // Offset for current line
	for (int i = 0; i < db->lines; i++) {
		texture_setX(db->textures[i], db->x + db->b + db->p);
		texture_setY(db->textures[i], db->y + db->b + db->p + offset);
		texture_render(db->textures[i]);
		offset += texture_getHeight(db->textures[i]) + db->s;
	}
}

/**
 * Set label and value of one of the next line in the dictbox
 * @param db 		dictbox structure
 * @param label		label of line
 * @param value_ptr	pointer to variable storing the value to display
 */
void dictbox_setLine(dictbox_t* db, char* label, int* value_ptr) {
	if (db->current_pop == db->lines) {
		printf(PRT_WARN"Cannot assign line to dictbox because it is already full \n");
	} else {
		db->labels[db->current_pop] = label;
		db->values[db->current_pop] = value_ptr;
		db->current_pop += 1;
	}
}

void dictbox_free(dictbox_t* db) {

	for(int i = 0; i < db->lines; i++) {
		texture_free(db->textures[i]);
	}

	free(db->labels);
	free(db->values);
	free(db);

}
