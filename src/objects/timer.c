#include "planetary.h"

sdlTimer_t* timer_init(){
	sdlTimer_t* t = (sdlTimer_t*) malloc(sizeof(sdlTimer_t));
	if (t == NULL) {
		printf("Error allocating memory for timer! \n");
		exit(EXIT_FAILURE);
	}
	return t;
}

void timer_start(sdlTimer_t* tim) {
    //Start the timer
    tim->mStarted = true;

    //Unpause the timer
    tim->mPaused = false;

    //Get the current clock time
    tim->mStartTicks = SDL_GetTicks();
	tim->mPausedTicks = 0;
}


void timer_stop(sdlTimer_t* tim) {
    //Stop the timer
    tim->mStarted = false;

    //Unpause the timer
    tim->mPaused = false;

	//Clear tick variables
	tim->mStartTicks = 0;
	tim->mPausedTicks = 0;
}

void timer_pause(sdlTimer_t* tim) {
    //If the timer is running and isn't already paused
    if( tim->mStarted && !tim->mPaused ) {
        //Pause the timer
        tim->mPaused = true;

        //Calculate the paused ticks
    	tim-> mPausedTicks = SDL_GetTicks() - tim->mStartTicks;
		tim->mStartTicks = 0;
    }
}

void timer_unpause(sdlTimer_t* tim) {
    //If the timer is running and paused
    if( tim->mStarted && tim->mPaused ) {
        //Unpause the timer
        tim->mPaused = false;

        //Reset the starting ticks
        tim->mStartTicks = SDL_GetTicks() - tim->mPausedTicks;

        //Reset the paused ticks
        tim->mPausedTicks = 0;
    }
}

Uint32 timer_getTicks(sdlTimer_t* tim) {
	//The actual timer time
	Uint32 time = 0;

    //If the timer is running
    if( tim->mStarted ) {
        //If the timer is paused
        if( tim->mPaused ) {
            //Return the number of ticks when the timer was paused
            time = tim->mPausedTicks;
        } else {
            //Return the current time minus the start time
            time = SDL_GetTicks() - tim->mStartTicks;
        }
    }

    return time;
}

bool timer_isStarted(sdlTimer_t* tim) {
	//Timer is running and paused or unpaused
    return tim->mStarted;
}

bool timer_isPaused(sdlTimer_t* tim) {
	//Timer is running and paused
    return (tim->mPaused && tim->mStarted);
}