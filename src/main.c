#include "planetary.h"

// Variables associated with window management
SDL_Window* 	gWindow;
SDL_Renderer* 	gRenderer;

// Variables used for aesthetics
TTF_Font* 	font_small;
TTF_Font* 	font_big;
SDL_Color 	color_background	= {105,    25,  107, 0xFF}; 
SDL_Color 	color_white 		= {0xFF, 0xFF, 0xFF, 0xFF};
SDL_Color 	color_black			= {0x00, 0x00, 0x00, 0xFF}; 	// 100% opaque
SDL_Color 	color_white_50 		= {0xFF, 0xFF, 0xFF, 128 };  	// 50% opaque
SDL_Color	color_blue			= {43, 	 156,  227,  0xFF};
SDL_Color 	color_green			= {70, 	 212,  122,  0xFF};
SDL_Color 	color_red			= {255,	  97,  113,  0xFF};
SDL_Color 	color_bluegrey		= {141,  149,  179,  0xFF};
SDL_Color 	color_bluewhite		= {160,  172,  242,  0xFF};

// Window parameters (will be overriden by config)
int 		windowWidth				= WINDOW_WIDTH;
int 		windowHeight			= WINDOW_HEIGHT;
int 		vsync					= (int) VSYNC;
int 		fpsMax					= FPS_MAX;
Uint32 		TPF 					= (Uint32) (1000 / FPS_MAX);
char 		windowTitle[STRLEN]		= WINDOW_TITLE;

// FPS variables
float 		avgFPS;				// FPS averaged over frame sample size
int			intFPS;				// integer converted avgFPS
int 		countedFrames;		// Cumulative frame count (to sample FPS over)
sdlTimer_t* fpsTimer;			// Timer used to calculate FPS

// FPS capping variables
Uint32 		frameTicks = 30;	// Used to cap FPS
Uint32		deltaTime = 0;		// Time elapsed since previous update
Uint32  	oldTime;			// Used in calculation of deltaTime
sdlTimer_t* capTimer;			// Timer used to cap FPS


int main( int argc, char* argv[] ) {	
	printf(PRT_SPACE"Start \n");

	// Read and apply config

	char confLocation[STRLEN] = DEFAULT_CONF;
	bool readConf = false;
	if (argc == 1) {
		// No config passed as argument
		if (access( confLocation, F_OK ) != -1) {
			// Config found in default location
			printf(PRT_MSG"Found config at '%s' \n", confLocation);
			readConf = true;
		} 		
	} else if (argc == 2){
		// Using specified config
		readConf = true;
		strcpy(confLocation, argv[1]);
		printf(PRT_MSG"Using config at '%s' \n",argv[1]);
	}

	if (readConf) {
		// Read and apply config
		config_t* config = config_read(confLocation);

		windowWidth = config->windowWidth;
		windowHeight = config->windowHeight;
		strcpy(windowTitle, config->windowTitle);
		vsync = config->vsync;
		fpsMax = config->fpsMax;
		TPF = (int) (1000 / fpsMax);
		free(config);
	} else {
		printf(PRT_ERROR"No config found! \n");
	}

	// Start up SDL and create window
	init();
	printf(PRT_MSG"Initialised SDL, etc. \n");

	// Prepare user-facing stuff, as well as game related stuff
	printf(PRT_SPACE"Preparing game\n");

	oldTime = SDL_GetTicks();
	fpsTimer = timer_init();
	capTimer = timer_init();
	countedFrames = 0;
	avgFPS = fpsMax;
	timer_start(fpsTimer);
	preLoop();

	// Begin game loop
	printf(PRT_SPACE"Starting game\n");

	bool doloop = true;
	while (doloop){
		deltaTime = SDL_GetTicks() - oldTime;
		oldTime = SDL_GetTicks();
		timer_start(capTimer);

		doloop = mainLoop();

		//Calculate and correct fps
		++countedFrames;
		if (countedFrames == FPS_SAMPLE) {
			avgFPS = countedFrames / ( timer_getTicks(fpsTimer) / 1000.f );
			
			if( avgFPS > FPS_ERRORABOVE ) {
				avgFPS = 0;
				printf(PRT_WARN"FPS calculated to be greater than %f \n", avgFPS );
			}

			intFPS = (int) roundf(avgFPS);

			countedFrames = 0;
			timer_start(fpsTimer);
		}

		// Handle FPS stuff
		frameTicks = timer_getTicks(capTimer);
		if (vsync != true) {
			if( frameTicks < TPF) {
				//Wait remaining time
				SDL_Delay( TPF - frameTicks );
			}
		}
	}


	printf(PRT_MSG"Game is exiting \n");

	// Once game loop stops, this is run to clean up (free memory, etc.)
	if (postLoop() == EXIT_FAILURE) {
		printf(PRT_WARN"Failed to clean up on exit! \n");
	}
	
	// Quit SDL
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;
	
	IMG_Quit();
	Mix_Quit();
	SDL_Quit();

	printf(PRT_SPACE"Goodbye \n");

	return EXIT_SUCCESS;
}

void init() {

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
		printf(PRT_ERROR"SDL could not be initialised! '%s' \n", SDL_GetError() );
		exit(EXIT_FAILURE);
	} else {
		//Create window
		gWindow = SDL_CreateWindow(windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN );
		if( gWindow == NULL ) {
			printf(PRT_ERROR"Window could not be created! '%s' \n",SDL_GetError() );
			exit(EXIT_FAILURE);
		} else {
			//Create vsynced renderer for window
			if (vsync) {
				gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			} else {
				gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			}
			
			
			if( gRenderer == NULL ) {
				printf(PRT_WARN"Renderer could not be created! '%s' \n", SDL_GetError() );
			} else {
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); // set default color to white
				SDL_SetRenderDrawBlendMode(gRenderer, SDL_BLENDMODE_BLEND);
				SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);
				SDL_RenderSetIntegerScale(gRenderer, 1);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
					printf(PRT_ERROR"SDL_image could not be initialised! '%s' \n", IMG_GetError() );
					exit(EXIT_FAILURE);
				}

				//Initialize SDL_ttf
                if( TTF_Init() == -1 ) {
                    printf(PRT_ERROR"SDL_ttf could not be initialised! '%s' \n", TTF_GetError() );
					exit(EXIT_FAILURE);
                } else {
					font_small = TTF_OpenFont( "./res/fonts/roboto_mono_medium.ttf", 13 ); 
					if( font_small == NULL ) {
						printf(PRT_ERROR"Failed to load small font! '%s' \n", TTF_GetError() );
						exit(EXIT_FAILURE);
					} 

					font_big = TTF_OpenFont( "./res/fonts/roboto_mono_medium.ttf", 22 ); 
					if( font_big == NULL ) {
						printf(PRT_ERROR"Failed to load big font! '%s' \n", TTF_GetError() );
						exit(EXIT_FAILURE);
					} 

				}

				if (!TEXTURE_ENABLE) {
					printf(PRT_WARN"Texture rendering disabled! \n");
				}

				if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 ) {
					printf(PRT_ERROR"SDL_mixer could not be initialised! '%s' \n", Mix_GetError() );
					exit(EXIT_FAILURE);
				}

			}
		}
	}
}