#include "planetary.h"
#include "globalvars.h"

/**
 * This file (util.c) provides utility functions, to be used globally.
 */


/**
 * Returns a random float within range
 * @param min		Minimum of range
 * @param max		Maximum of range
 */
float getRand(float min, float max){
	float out = rand() /  ( (float) RAND_MAX ) * (max - min) + min;
	return out;
}

/**
 * Returns array of floats, linearly spaced (range limits are inclusive)
 * @param min		Minimum of range
 * @param max		Maximum of range
 * @param n			Number of values within range
 */
float* linspace(float min, float max, int n) {
	float* linspace = (float*) calloc(n, sizeof(float));

	// printf("linspace: %g %g \n", min, max);
	for (int i = 0; i < n; i++) {
		linspace[i] = (float) (min + i*(max-min)/(n-1));
		// printf("%g, ", linspace[i]);
	}

	// printf("\n");

	return linspace;	
}


void flatten2D(pix_t* out, pix_t** src, int w, int h) {
	int k = 0;
	for (int i = 0; i < h; i++ ) {		// along each row
		for (int j = 0; j < w; j++) {	// for each element in this row
			out[k] = src[i][j];
			k++;
		}
	}

}

void unflatten2D(pix_t** out, pix_t* src, int w, int h) {
	int len = w*h;

	int i = 0; // row 
	int j = 0; // row element
	for(int k = 0; k < len; k++) {
		if (j % w == 0) { // end of row
			i += 1; // go to next row
			j  = 0; // go to start of row
		}
		out[i][j] = src[k];
	}

}


void weightedSum2D(pix_t* A, pix_t* B, int len, int w1, int w2) {
	int d = w1 + w2;
	for (int i = 0; i < len; i++) {

		Uint8 Aa = ((A[i] >> 24) & 0xFF) / 255.0;		// Extract the AA byte
		Uint8 Ar = ((A[i] >> 16) & 0xFF) / 255.0; 		// Extract the RR byte
		Uint8 Ag = ((A[i] >> 8) & 0xFF) / 255.0; 		// Extract the GG byte
		Uint8 Ab = ((A[i]) & 0xFF) / 255.0;		 		// Extract the BB byte

		Uint8 Ba = ((B[i] >> 24) & 0xFF) / 255.0;		// Extract the AA byte
		Uint8 Br = ((B[i] >> 16) & 0xFF) / 255.0; 		// Extract the RR byte
		Uint8 Bg = ((B[i] >> 8) & 0xFF) / 255.0; 		// Extract the GG byte
		Uint8 Bb = ((B[i]) & 0xFF) / 255.0;		 		// Extract the BB byte

		Uint8 Ca = (Aa*w1 + Ba*w2) / d;
		Uint8 Cr = (Ar*w1 + Br*w2) / d;
		Uint8 Cg = (Ag*w1 + Bg*w2) / d;
		Uint8 Cb = (Ab*w1 + Bb*w2) / d;

		pix_t out = 0;
		int bitshift = 8;

		out = Ca;
		out = (out << bitshift) + Cr;
		out = (out << bitshift) + Cg;
		out = (out << bitshift) + Cb;

		A[i] = out;

	}
}