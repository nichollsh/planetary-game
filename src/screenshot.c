#include "planetary.h"
#include "globalvars.h"

/**
 * This file (screenshot.c) implements code providing screenshot functionality.
 * The contents of the game window are saved to bitmap file
 */

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    Uint32 rmask = 0xff000000;
    Uint32 gmask = 0x00ff0000;
    Uint32 bmask = 0x0000ff00;
    Uint32 amask = 0x000000ff;  
#else
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
#endif


/**
 * Take screenshot of renderer and saves to file in: '#define SCREENSHOTS'
 * @param gRenderer SDL Renderer to be screenshotted
 */
void screenshot(SDL_Renderer* gRenderer) {

	char scpath[STRLEN] = "";

	int t = (int)time(NULL);
	char timestr[16] = "";
	sprintf(timestr, "%d", t);

	strcat(scpath, SCREENSHOTS"screenshot_");
    strcat(scpath,timestr);
	strcat(scpath, ".bmp");

	printf(PRT_MSG"Taking a screenshot '%s' \n", scpath);

	SDL_Surface* surf = SDL_CreateRGBSurface(0,windowWidth,windowHeight,32,rmask, gmask, bmask, amask); 

	SDL_LockSurface(surf);
	SDL_RenderReadPixels(gRenderer,NULL,surf->format->format, surf->pixels,surf->pitch);

	SDL_SaveBMP(surf,scpath); 
	SDL_UnlockSurface(surf);
	SDL_FreeSurface(surf);
}