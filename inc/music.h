#include "planetary.h"

typedef struct ms {

	Mix_Music* music;

} music_t;

music_t* 	music_loadFromFile(char* file);
void 		music_free(music_t* ms);
void 		music_start(music_t* ms, int loopNumber);
void 		music_startf(music_t* ms, int loopNumber);
void 		music_toggle();
void		music_stop();
