#include "planetary.h"

typedef struct {

	int				ID; // Integer ID associated with this state, from the enum in 'planetary.h'
	bool			_ready; // Is this gamestate ready to be made current, or do things need loading?

	button_t*		buttonPool[MAX_BUTTONS];			// Pool containing buttons in this state
	int 			buttonLayers[MAX_BUTTONS];			// Layer of each button with shared index as above
	int				button_pop;

	texture_t*		texturePool[MAX_TEXTURES];		// Pool containing image textures in this state
	int 			textureLayers[MAX_TEXTURES];	// Layer of each IT with shared index as above
	int				texture_pop;

	sound_t*		soundPool[MAX_SOUNDS];				// Pool containing sound effects in this state
	int				sound_pop;

	pixelarray_t*	pixelarrayPool[MAX_PIXELARRAYS];	// Pool containing pixel arrays in this state
	int 			pixelarrayLayers[MAX_PIXELARRAYS];	// Layer of each PA with shared index as above
	int				pixelarray_pop;

	rectangle_t*	rectanglePool[MAX_RECTANGLES];		// Pool containing rectangles in this state
	int 			rectangleLayers[MAX_RECTANGLES];	// Layer of each rect with shared index as above
	int				rectangle_pop;

} gamestate_t;

// Generalised gamestate stuff

gamestate_t*	gamestate_init();
void			gamestate_free(int ID);
void			gamestate_reset(int ID);

void 			gamestate_registerButton(gamestate_t* gs, button_t* button, int layer);
void			gamestate_registerTexture(gamestate_t* gs, texture_t* tex, int layer);
void 			gamestate_registerPixelarray(gamestate_t* gs, pixelarray_t* pa, int layer);
void 			gamestate_registerRectangle(gamestate_t* gs, rectangle_t* rect, int layer);
void			gamestate_registerSound(gamestate_t* gs, sound_t* sd);

void			gamestate_setReady(int ID);
void 			gamestate_setNext(int ID);
bool 			gamestate_isReady(int ID);

void 			gamestate_update(gamestate_t* gs);
void 			gamestate_render(gamestate_t* gs);

gamestate_t* 	gamestate_getCurrent();
void 			gamestate_makeCurrent(int ID);
gamestate_t* 	gamestate_getPrevious();
gamestate_t*	gamestate_getNext(); 


// Specific gamestate stuff

bool			gsf_ingame_init();
void			gsf_ingame_logic();