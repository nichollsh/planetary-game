#!/usr/bin/env python

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import os

# cmap = cm.get_cmap('terrain_r')
cmap = cm.get_cmap('gist_earth_r')

rng = np.linspace(0,0.85,20)
rdp = 0

print("Length = " + str(len(rng)))

print("Cmap data...")

for i in rng:
    r = int(round(cmap(i)[0] * 255,rdp))
    g = int(round(cmap(i)[1] * 255,rdp))
    b = int(round(cmap(i)[2] * 255,rdp))
    
    print("{0xFF," + str(r) + "," + str(g) + "," + str(b) + "}",end='')

    if not (i == rng[-1]):
        print(",",end='')
    
print("\n")

fig = plt.figure(figsize=(11,2.2))
ax = fig.add_subplot(111)

ax.scatter(rng,np.ones(np.shape(rng)),c=[cmap(i) for i in rng],marker='s',s=250)
ax.get_yaxis().set_visible(False)
ax.set_xticks(np.arange(0,1.1,0.1))

plt.show()
