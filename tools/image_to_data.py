#!/usr/bin/env python

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import time
import progressbar
import os

Image.MAX_IMAGE_PIXELS = 93312000000

path = "/home/harrison/programming/planetary-game/res/planet_maps/mars/"
folder = "gen_dem/"
header = "PixelArray chunked data (dem)"
filename = "dem_20k.tif"

top_crop = 0
bot_crop = 0

tiles_wide = 20
tiles_tall = 10

# tiles_wide = 1
# tiles_tall = 1

im_file = Image.open(path + filename)
im_all = np.array(im_file)

im_all = np.delete(im_all,range(0,top_crop),axis=0)
im_all = np.delete(im_all,range(len(im_all)-bot_crop,len(im_all)),axis=0)

width = len(im_all[0])
height = len(np.transpose(im_all)[0])

tile_width  = int(width  / tiles_wide)
tile_height = int(height / tiles_tall)

im_min = np.amin(im_all)
print("min:" + str(im_min))
im_max = np.amax(im_all)
print("max:" + str(im_max))

downsample = 1
dp = 4

write = True

if write:
	print("Write = True")
else:
	print("Write = False")

print("Tiles wide: " + str(tiles_wide))
print("Tiles tall: " + str(tiles_tall))
print("Downsample: " + str(downsample))

print("Normalise....")

im_all = np.around((im_all - im_min) / im_max,dp)

print("Chunk...")

if not write:
	fig = plt.figure(figsize=(20,10))
	ax = []

k = 0
bar = progressbar.ProgressBar(max_value=tiles_tall*tiles_wide)

for i in range(0,tiles_tall): # for each row
	for j in range(0,tiles_wide): # for each element in this row
		
		# print("k = " + str(k))
		
		if (tiles_wide == 1) and (tiles_tall == 1):
			im_part = im_all
		else:
			im_part = im_all[i*tile_height:(i+1)*tile_height,j*tile_width:(j+1)*tile_width]
		
		
		if (downsample > 1):
			im_part = im_part[::downsample,::downsample]

		if (write == False):
			
			ax.append(fig.add_subplot(tiles_tall,tiles_wide,k+1))
			ax[k].get_xaxis().set_visible(False)
			ax[k].get_yaxis().set_visible(False)
			

			im_part = np.array(im_part)
			
			ax[k].imshow(im_part,plt.get_cmap('gray'),interpolation='nearest',vmin=0,vmax=1)

		else:
			
			part_x = tile_width / downsample
			part_y = tile_height / downsample
			
			chunk_file = str(k) + ".pa"
			p = path + folder + chunk_file
			
			if os.path.exists(p):
				  os.remove(p)

			f = open(p, "w")
			f.write(header+"\n")       # Header info
			f.write(str(k) + "\n")              # Index
			f.write(str(int(part_x)) + "\n")    # Width
			f.write(str(int(part_y)) + "\n")    # Height

			warray = im_part.flatten()
			
			for i2 in range(len(warray)):
				f.write(str(warray[i2]) + "\n")

			f.close()
		
		bar.update(k)
		k = k + 1

if not write:
	print("Plot...")
	plt.tight_layout()
	plt.show()

print("Done!")