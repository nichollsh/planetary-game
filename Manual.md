# Manual on how to play the game
Note that the game should be intiutive to play. This is mostly just to note down key points.

## Key bindings

* Global 
	* `F2` - Take screenshot
	* `F3` - Show debug
	* `F7` - Toggle music

* In-game
	* `esc` - Open pause menu
	* `w`	- Pan up
	* `a`	- Pan left
	* `s`	- Pan down
	* `d`	- Pan right
	* `z`	- Zoom out
	* `x`	- Zoom in
